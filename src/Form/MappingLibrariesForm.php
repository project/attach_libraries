<?php

namespace Drupal\attach_libraries\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MappingLibrariesForm.
 */
class MappingLibrariesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'attach_libraries.mappinglibraries',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mapping_libraries_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('attach_libraries.mappinglibraries');
    $form['libraries'] = [
      '#type' => 'details',
      '#title' => t('Map Libraries'),
    ];
    $form['libraries']['block_libraries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapped Libraries for Block'),
      '#default_value' => $config->get('block_libraries'),
      '#description' => t('Enter Libraries Mapping in Json Format. Json format is <b><pre>{"BLOCK_ID-1" : ["LIBRARY-1", "LIBRARY-2", "LIBRARY-3"]}</pre></b>'),
      '#element_validate' => [
              [
                'Drupal\attach_libraries\Validate\JsonValidate',
                'validate',
              ],
      ],
    ];
    $form['libraries']['path_libraries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapped Libraries for Path'),
      '#default_value' => $config->get('path_libraries'),
      '#description' => t('Enter Libraries Mapping in Json Format. Json format is <b><pre>{"PATH-1" : ["LIBRARY-1", "LIBRARY-2", "LIBRARY-3"]}</pre></b>'),
      '#element_validate' => [
              [
                'Drupal\attach_libraries\Validate\JsonValidate',
                'validate',
              ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('attach_libraries.mappinglibraries')
      ->set('block_libraries', $form_state->getValue('block_libraries'))
      ->set('path_libraries', $form_state->getValue('path_libraries'))
      ->save();
  }

}
