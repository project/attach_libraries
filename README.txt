
INTRODUCTION:
------------
This module helps to attach libraries to view displays, blocks, node and
taxonomy pages as well.

For a full description of the module, visit the project page:

https://www.drupal.org/project/attach_libraries

INSTALLATION:
-------------
1. Place the entire attach libraries folder into your Drupal sites/all/modules/
   directory.

2. Enable the attach libraries module by navigating to:

     administer > modules

IMPORTANT NOTE:
---------------
1. You can attach libraries either from the module or from the theme.

2. To attach libraries to views, use the below key value format,
	 "views_block:view_display_name" : ["themename/libraryname"]

3. To attach libraries to pages(node or taxonomy), use the below key value format,
	 "/taxonomy/term/tid" : ["themename/libraryname"]
	 "/node/nid" : ["themename/libraryname"]

4. To attach same library to multiple nodes, use the below format,
	 "/node/nid\n/node/nid" : ["themename/libraryname"]

5. You can also use module name instead of theme name if the library is from a module.

Author:
-------
Jack Ry
